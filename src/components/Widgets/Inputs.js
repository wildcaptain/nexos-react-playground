import React from "react";
import styled, { css } from "styled-components"
import {DARK_TEXT_COLOR, LIGHT_TEXT_COLOR} from "../../utils/constants";


export const SingleLineInput = ({className, placeholder="", type="text", name="", register=function(){}})=> {
  return (
    <StyledSingleLineInput
      className={className}
      type={type}
      placeholder={placeholder}
      {...register(name)}
    />
  )
}

const StyledSingleLineInput = styled.input`
  width: 100%;
  min-width: 150px;
  border-radius: 5px;
  border: 1px solid hsl(0,0%,40%);
  color: ${DARK_TEXT_COLOR};
  padding: 5px 7px;
  transition: all 0.2s ease-in;
`;


export const LargeInput = ({className, placeholder="", type="text", name="", register=function(){}})=> {
  return (
    <StyledLargeInput
      className={className}
      type={type}
      placeholder={placeholder}
      {...register(name)}
    />
  )
}

const StyledLargeInput = styled.input`
  width: 100%;
  min-width: 150px;
  height: 50px;
  border-radius: 5px;
  border: 1px solid hsl(0,0%,95%);
  box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.25);
  color: ${LIGHT_TEXT_COLOR};
  padding: 12px 15px;
  transition: all 0.2s ease-in;
`;