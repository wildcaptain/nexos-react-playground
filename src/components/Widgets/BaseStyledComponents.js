import React from "react";
import styled, {css} from 'styled-components/macro'
import {HashLink} from "react-router-hash-link";
import {DARK_TEXT_COLOR, PRIMARY_COLOR} from "../../utils/constants";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


export const BaseButton = styled.button`
  background-color: ${PRIMARY_COLOR};
  color: white;
  border: 1px solid #DBE2E9;
  font-size: 0.9em;
  font-weight: 700;
  border-radius: 3px;
  padding: 6px 12px;
  ${props => props.secondary && css`
    background-color: white;
    color: #000104;
  `}
`;

export const IconButton = styled.button`
  color: #5f5f5f;
  background-color: transparent;
  border: none;
  font-size: 16px;
  font-weight: 500;
  padding: 2px;
  &:hover {
    color: ${PRIMARY_COLOR};
  }
`;

export const FancyButton = styled(HashLink)`
  width: auto;
  display: inline-flex;
  color: #fff;
  background-color: ${PRIMARY_COLOR};
  align-items: center;
  justify-content: center;
  position: relative;
  line-height: 1.25;
  padding: calc(.875rem - 1px) calc(1.5rem - 1px);
  min-height: 2rem;
  font-weight: 600;
  border: 1px solid transparent;
  border-radius: .25rem;
  box-shadow: 0 1px 3px 0 rgba(0,0,0,.02);
  transition: all 250ms ease;
  text-decoration: none;
  white-space: nowrap;
  &:hover {
    transform: translateY(-1px);
    background-color: ${props => (props.secondary ? "#fff" : `${PRIMARY_COLOR}`)};
    color: ${props => (props.secondary ? `${PRIMARY_COLOR}` : "#fff")};
    border-color: ${props => (props.secondary ? "rgba(0,0,0,.15)" : props.borderColor)};
    box-shadow: ${props => (props.secondary ? "box-shadow: 0 4px 12px rgba(0,0,0,.1);" : props.boxShadow)};
  }
  ${props => props.secondary && css`
    color: rgba(0,0,0,.85);
    background-color: #fff;
    border-color: rgba(0,0,0,.1);
    box-shadow: none;
  `}
`;


export const FlexColumn = styled.div`
  display: flex;
  flex-direction: column;
`;

export const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
`;

export const GridParent = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-auto-flow: row;
`;

export const Text = styled.div`
  display: flex;
  font-style: normal;
  font-size: 14px;
`;

export const BoldedText = styled(Text)`
  font-weight: 600;
`;

export const ParagraphText = styled(Text)`
  font-weight: 400;
  white-space: pre-wrap;
`;

export const OverflowText = styled(ParagraphText)`
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const RightArrowIcon = styled(FontAwesomeIcon)`
  align-self: flex-start;
  margin: 2px 0 0 5px;
  color: #191919;
`;

export const LeftArrowIcon = styled(FontAwesomeIcon)`
  align-self: center;
  margin: 2px 0 0 5px;
  color: #191919;
  &:hover {
    cursor: pointer;
  }
`;