import React from "react";
import styled, { css } from "styled-components"
import {BLUE_TEXT_COLOR, DARK_TEXT_COLOR, LIGHT_TEXT_COLOR} from "../../utils/constants";


// Styled Components

export const PromptText = styled.div`
  color: ${BLUE_TEXT_COLOR};
  font-size: 14px;
  font-weight: 400;
`;

export const NameTitle = styled.div`
  color: ${DARK_TEXT_COLOR};
  font-size: 16px;
  font-weight: 700;
`;

export const FormFieldTitle = styled.h6`
  color: ${DARK_TEXT_COLOR};
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 5px;
`;

export const SectionSubTitle = styled.h6`
  width: 100%;
  text-align: left;
  font-weight: 500;
  color: ${LIGHT_TEXT_COLOR};
`;

// Custom Components
export const SectionTitle = ({children, classname, underlined=true}) => {
  return (
    <StyledSectionTitle classname={classname} underlined={underlined}>
      {children}
    </StyledSectionTitle>
  )
}

const StyledSectionTitle = styled.h5`
  width: 100%;
  text-align: left;
  font-weight: 600;
  ${props => props.underlined && css`
    border-bottom: 0.5px solid lightgrey;
  `}
`;

