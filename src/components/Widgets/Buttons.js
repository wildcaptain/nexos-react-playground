import React from "react";
import styled, {css} from "styled-components/macro";
import {DARK_TEXT_COLOR, PRIMARY_COLOR} from "../../utils/constants";
import {HashLink} from "react-router-hash-link";


// Rectangular Buttons

const StyledButton = styled.button`
  background-color: ${PRIMARY_COLOR};
  color: white;
  font-size: 15px;
  font-weight: 700;
  border-radius: 3px;
  border: none;
  padding: 3px 15px;
  box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.25);
  white-space: nowrap;
  text-align: center;
  cursor: pointer;
  text-decoration: none;
  ${props => props.secondary && css`
    background-color: white;
    color: ${DARK_TEXT_COLOR};
  `};
  ${props => props.size === "large" && css`
    width: 100%;
    padding: 5px 15px;
  `};
  ${props => props.shape === "rounded" && css`
    border-radius: 20px;
  `};

  @media (min-width: 600px) {
    max-width: 400px;
  }
`

export const Button = ({children, size="small", shape="rectangle"}) => {
  return (
    <StyledButton size={size} shape={shape}>
      {children}
    </StyledButton>
  )
}



// Homepage Buttons

const buttonStyles = `
  width: 100%;
  height: 50px;
  background-color: ${PRIMARY_COLOR};
  color: white;
  font-size: 14px;
  font-weight: 700;
  border-radius: 5px;
  border: none;
  padding: 15px 25px;
  box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.25);
  white-space: nowrap;
  text-align: center;
  cursor: pointer;
  text-decoration: none;
  margin-bottom: 10px;
  ${props => props.secondary && css`
    background-color: white;
    color: #000104;
  `}
  ${props => props.size === "small" && css`
    font-size: 12px;
    height: 20px;
  `}
  
  @media (min-width: 600px) {
    max-width: 175px;
  }
`

export const HomePageButton = ({
  children,
  className,
  icon=null,
  type,
  onClick,
  style,
  size="large",
  link=false,
  to
}) => {
  return (
    <>
      {link
        ? <StyledLinkButton className={className} size={size} to={to} onClick={onClick} type={type}>
          {icon}
          {children}
        </StyledLinkButton>
        : <StyledFancyButton className={className} size={size} onClick={onClick} type={type}>
          {icon}
          {children}
        </StyledFancyButton>
      }
    </>
  )
}


const StyledFancyButton = styled.button`
  ${buttonStyles}
`;

const StyledLinkButton = styled(HashLink)`
  ${buttonStyles}
`;