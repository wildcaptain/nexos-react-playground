import React from "react";
import styled from "styled-components";
import {FormFieldTitle} from "./Text";
import {FlexColumn} from "./BaseStyledComponents";
import {SingleLineInput} from "./Inputs";

export const SingleLineFormField = ({title, defaultValue="", placeholder="", name="", register}) => {
  return (
    <StyledFormContainer>
      <FormFieldTitle>{title}</FormFieldTitle>
      <SingleLineInput default={defaultValue} placeholder={placeholder} name={name} register={register}/>
    </StyledFormContainer>
  )
}

const StyledFormContainer = styled(FlexColumn)`
  padding-bottom: 20px;
`;

