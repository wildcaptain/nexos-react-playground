import React, {useEffect, useState} from "react";
import {Link, NavLink as RouterNavLink, useHistory} from "react-router-dom";
import { AiOutlineClose } from 'react-icons/ai'
import { IconContext } from 'react-icons';
import styled, { css } from 'styled-components/macro';

import {BaseButton, FancyButton, FlexColumn, FlexRow} from "./Widgets/BaseStyledComponents";
import {DARK_COLOR_VARIATION, DARK_TEXT_COLOR, PRIMARY_COLOR, SIDEBAR_WIDTH, TOP_BAR_HEIGHT} from "../utils/constants"
import {
  Nav,
  NavItem,
} from "reactstrap";
import {HashLink} from "react-router-hash-link";


const NavBar = ({toggleSidebar, sidebar }) => {
  const handleLogInButton = () => {
    console.log("LOGGED IN")
  };

  const handleSignUpButton = () => {
    console.log("SIGNED UP")
  };

  return (
    <>
    <IconContext.Provider value={{color: '#fff'}}>
      <TopNavBar>
        <LogoContainer>
          <HashLink to="/#">
            <LogoImage src={"https://storage.googleapis.com/nexos-branding/icons/NexosLogoLongText.svg"} alt={"Long Logo"}/>
          </HashLink>
        </LogoContainer>
        <SimpleLogoContainer>
          <HashLink to="/#">
            <LogoImage src={"https://storage.googleapis.com/nexos-branding/icons/NexosLogoShortText.svg"} alt={"Short Logo"}/>
          </HashLink>
        </SimpleLogoContainer>
        <NavBarMenuItems>
          <LinkItem to="/#for-patients">Patients</LinkItem>
          <LinkItem to="/#for-gps">GPs</LinkItem>
          <LinkItem to="/#for-health-professionals">Health Professionals</LinkItem>
          <LinkItem to="/#about">About</LinkItem>
          <LinkItem to="/#contact-us">Contact</LinkItem>
        </NavBarMenuItems>
        <ActionsContainer>
          <StyledNav className="d-md-block" navbar>
            <NavButtonsContainer>
              <StyledNavItem>
                <FancyButtonNavBar secondary onClick={handleLogInButton}>Log In</FancyButtonNavBar>
              </StyledNavItem>
              <StyledNavItem>
                <FancyButtonNavBar onClick={handleSignUpButton}>Sign Up</FancyButtonNavBar>
              </StyledNavItem>
            </NavButtonsContainer>
          </StyledNav>
        </ActionsContainer>
      </TopNavBar>
    </IconContext.Provider>
    </>
  );
};

export default NavBar;

const TopNavBar = styled(FlexRow)`
  background-color: #fefefe;
  height: ${TOP_BAR_HEIGHT};
  justify-content: space-between;
  align-items: center;
  position: fixed;
  width: 100%;
  padding: 0 20px;
  z-index: 2;
  box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
`;

const StyledNavItem = styled(NavItem)`
`;

const FancyButtonNavBar = styled(FancyButton)`
  padding: calc(.575rem - 1px) calc(1.25rem - 1px);
  ${props => props.secondary && css`
    border: none;    
  `}
`;

const NavMenu = styled.nav`
  background-color: #f8f9fa;
  width: ${SIDEBAR_WIDTH};
  height: 100vh;
  display: flex;
  justify-content: center;
  position: fixed;
  top: ${TOP_BAR_HEIGHT};
  left: -100%;
  transition: 850ms;
  z-index: 1;
  box-shadow: 2px 5px 3px 0 rgba(0,0,0,0.16);
  ${props => props.active && css`
    left: 0;
    transition: 350ms;
  `}
`;

const NavText = styled.li`
  display: flex;
  justify-content: start;
  align-items: center;
  padding-left: 16px;
  list-style: none;
  height: 50px;
`;

const NavTextLink = styled(Link)`
  color: #394963;
  font-size: 16px;
  font-weight: 600;
  width: 95%;
  height: 100%;
  display: flex;
  align-items: center;
  padding: 0 16px;
  border-radius: 4px;
  &:hover {
    background-color: #ebebeb;
    text-decoration: none;
  }
`;

const SideBarMenuItems = styled.ul`
  width: 100%;
  padding-inline-start: 0;
  padding-top: 25px;
`;

const TitleSpan = styled.span`
  margin-left: 16px;
`;

export const LogoImage = styled.img`
  height: 35px;
  @media (min-width: 800px) {
    content: url(https://storage.googleapis.com/nexos-branding/icons/NexosLogoLongText.svg);
  }
`;

const LogoContainer = styled.div`
  padding-left: 10px;
  @media (max-width: 800px) {
    display: none;
  }
`;

const SimpleLogoContainer = styled.div`
  @media (min-width: 800px) {
    display: none;
  }
`;

const NavBarMenuItems = styled(FlexRow)`
  justify-content: space-between;
  flex-grow: 1;
  max-width: 500px;
  padding: 0 25px;
  @media (max-width: 800px) {
    display: none;
  }
`;

const LinkItem = styled(HashLink)`
  flex-grow: 0;
  font-size: 14px;
  font-weight: 700;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.76;
  letter-spacing: normal;
  text-align: right;
  color: ${DARK_TEXT_COLOR};
  padding-right: 14px;
  white-space: nowrap;
  @media (max-width: 800px) {
    display: none;
  }
`;

const NavButtonsContainer = styled(FlexRow)`
  padding-top: 0.5em;
`;

const ActionsContainer = styled.div`
  height: 100%;
`;

const StyledNav = styled(Nav)`
  height: 100%;
`;
