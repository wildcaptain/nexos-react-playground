import React, {useState, useEffect} from "react";
import { Router, Route, Switch, useHistory } from "react-router-dom";
import styled, { css } from 'styled-components/macro';

import {FlexColumn, FlexRow} from "./components/Widgets/BaseStyledComponents";
import {FOOTER_HEIGHT, SIDEBAR_WIDTH, TOP_BAR_HEIGHT} from "./utils/constants";
import Footer from "./components/Footer";
import {FindMyProfile} from "./containers/onboarding/FindMyProfile";
import {CreateProfile} from "./containers/onboarding/CreateProfile";
import {CreateAccount} from "./containers/onboarding/CreateAccount";
import {OnboardingComplete} from "./containers/onboarding/OnboardingComplete";
import NavBar from "./components/NavBar";
import {Home} from "./containers/Home";


const App = () => {
  // const { loading } = useAuth();
  const [sidebar, setSidebar] = useState(false);

  const toggleSidebar = () => {
    setSidebar(!sidebar)
  };

  // if (loading) {
  //   return <Loading/>;
  // }

  return (
    <AppContainer>
      <NavBar toggleSidebar={toggleSidebar} sidebar={sidebar}/>
      <FlexColumn>
        <PageBodyContainer sidebar={sidebar}>
          <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/find-my-profile" exact render={(props) =>
              <FindMyProfile {...props}/>}/>
            <Route path="/create-profile" exact render={(props) =>
              <CreateProfile {...props}/>}/>
            <Route path="/create-account" exact render={(props) =>
              <CreateAccount {...props}/>}/>
            <Route path="/onboarding-complete" exact render={(props) =>
              <OnboardingComplete {...props}/>}/>
          </Switch>
        </PageBodyContainer>
        <Footer/>
      </FlexColumn>
    </AppContainer>
  );
};

export default App;

const PageBodyContainer = styled.div`
  position: relative;
  padding-top: ${TOP_BAR_HEIGHT};
  transition: 350ms;
  min-height: 100vh;
  ${props => props.sidebar && css`
    padding-left: ${SIDEBAR_WIDTH};  
    transition: 550ms;
  `}
`;

const AppContainer = styled(FlexColumn)`
  position: relative;
  min-height: 100vh;
`;

// const ContentWrap = styled.div`
//   padding-bottom: 35px;
// `;


