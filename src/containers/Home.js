import React from "react";
import {useHistory} from "react-router-dom";
import {Button} from "../components/Widgets/Buttons";

export const Home = () => {

  const history = useHistory()

  return (
    <div>
      <div>Hello this is the home screen</div>
      <Button onClick={history.push("/find-my-profile")}>Click</Button>
    </div>
  )
}