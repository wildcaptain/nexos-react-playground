import React, {useState, useEffect} from "react";
import {PromptText, SectionSubTitle, SectionTitle} from "../../components/Widgets/Text";
import {useHistory} from "react-router-dom";
import {SingleLineFormField} from "../../components/Widgets/FormFields";
import {useForm} from "react-hook-form";
import {Button} from "../../components/Widgets/Buttons";
import {
  ButtonContainer,
  InputsContainer,
  OnboardingForm,
  OnboardingSectionContainer,
  OnboardingSectionWrapper,
  TitleContainer
} from "./CreateProfile";



export const CreateAccount = ({...props}) => {
  const history = useHistory();
  const {register, handleSubmit, errors} = useForm()

  let firstName = (props.location.query && props.location.query.firstName) || ""
  let lastName = (props.location.query && props.location.query.lastName) || ""
  let ahpraNumber = (props.location.query && props.location.query.ahpraNumber) || ""

  const onSubmit = (data) => {
    console.log("ACCOUNT DATA", data)
    // dispatch(createAccount())
    history.push({
      pathname: "/onboarding-complete",
    })
  }

  return (
    <OnboardingSectionContainer>
      <OnboardingSectionWrapper>
        <TitleContainer>
          <SectionTitle underlined={false}>We are almost there!</SectionTitle>
          <SectionSubTitle>Enter your email and create a password to access your Nexos Health account</SectionSubTitle>
        </TitleContainer>
        <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
          <InputsContainer>
            <SingleLineFormField type="email" name="email" title="Email" register={register}/>
            <SingleLineFormField type="password" name="password" title="Password" register={register}/>
          </InputsContainer>
          <ButtonContainer>
            <Button size="large">Complete Registration</Button>
          </ButtonContainer>
        </OnboardingForm>
      </OnboardingSectionWrapper>
    </OnboardingSectionContainer>
  )
}
