import React, {useState, useEffect} from "react";
import styled, {css} from "styled-components/macro";
import {FlexColumn, FlexRow} from "../../components/Widgets/BaseStyledComponents";
import {SectionSubTitle, SectionTitle} from "../../components/Widgets/Text";
import {useHistory} from "react-router-dom";
import {useForm} from "react-hook-form";
import {Button} from "../../components/Widgets/Buttons";
import {OnboardingSectionContainer, OnboardingSectionWrapper} from "./CreateProfile";



export const OnboardingComplete = ({...props}) => {
  return (
    <OnboardingSectionContainer>
      <OnboardingSectionWrapper>
        <ImageContainer>
          <Image src="https://storage.googleapis.com/nexos-branding/icons/MultipleDoctors.svg" alt="medical-doctor"/>
        </ImageContainer>
        <TextContainer>
          <Title underlined={false}>Congratulations!</Title>
          <SubTitle>
            You have completed the sign up process. Over the next few days we will contact you to complete the
            verification process, after which you will be able to edit your professional profile.
          </SubTitle>
        </TextContainer>
        <Button size="large">Continue</Button>
      </OnboardingSectionWrapper>
    </OnboardingSectionContainer>
  )
}


const ImageContainer = styled(FlexRow)`
  width: 100%;
  justify-content: center;
  padding-bottom: 20px;
  max-width: 400px;
`;

const Image = styled.img`
  width: 80%;
`;

const TextContainer = styled(FlexColumn)`
  padding-bottom: 20px;
  text-align: center;
`;

const Title = styled(SectionTitle)`
  text-align: center;
`;

const SubTitle = styled(SectionSubTitle)`
  text-align: center;
`;
